@extends('layout')
@section('content')
    <div id="grid">
        <table class="table table-hover table-striped">
            <thead style="background: #eeeeee">
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>Nascimento</th>
                    <th>Gênero</th>
                    <th>Pais</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach($pessoas as $pessoa)
                <tr>
                    <td>{{ $pessoa->id }}</td>
                    <td>{{ $pessoa->nome }}</td>
                    <td>{{ $pessoa->nascimento->format('d/m/Y') }}</td>
                    <td>
                        @switch($pessoa->genero)
                            @case('M') Masculino @break
                            @case('F') Feminino @break
                            @default Não informado @break
                        @endswitch
                    </td>
                    <td>{{ $pessoa->pais->nome }}</td>
                    <td>
                        <a href="#" class="btn btn-sm btn-info">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-danger">
                            <i class="fas fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
