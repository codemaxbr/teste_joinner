<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $table = 'pessoa';

    protected $fillable = [
        'id',
        'nome',
        'nascimento',
        'genero',
        'pais_id'
    ];

    protected $casts = [
        'nascimento' => 'date'
    ];

    protected $dates = [
        'nascimento'
    ];

    public function pais()
    {
        return $this->belongsTo(Pais::class);
    }
}
