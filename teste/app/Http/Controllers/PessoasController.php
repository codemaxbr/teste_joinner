<?php

namespace App\Http\Controllers;

use App\Contracts\PessoaContract;
use Illuminate\Http\Request;

class PessoasController extends Controller
{
    private $pessoa;

    public function __construct(PessoaContract $pessoa)
    {
        $this->pessoa = $pessoa;
    }

    public function index()
    {
        $result = $this->pessoa->all();
        return view('grid', [
            'pessoas' => $result
        ]);
    }
}
