<?php

namespace App\Repositories;

use App\Contracts\PessoaContract;
use App\Models\Pessoa;

class PessoaRepository implements PessoaContract
{
    public function all()
    {
        return Pessoa::with('pais')->get();
    }
}
